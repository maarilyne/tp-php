<?php

require('php/Connexion.php');

 ?>

<!DOCTYPE html>
<html>
	<head>
		<link rel="shortcut icon" href="img/Books.jpg" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="bibliotheque.css">
		<title>Ma Bibliothèque</title>
		<meta charset="utf-8">

		<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Montserrat|Permanent+Marker&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Indie+Flower&display=swap" rel="stylesheet">
	</head>

	<body>
		<header>
			<h1>Bienvenue dans ma bibliothèque !</h1>
			<nav id="navbar">
				<ul>
					<li><a href="">Tous les livres</a></li>
					<li><a href="">Par auteur</a></li>
					<li><a href="">Par année</a></li>
					<li><a href="">Nouveau livre</a></li>
				</ul>
			</nav>
		</header>
		<main>
			<section id="grid-item1" class="grid-items">
				<div class="description">
					<img src="img/couvertures-livres/livre1.jpg" alt="Alice au royaume du trèfle">
					<p>
						<!--*INSERT INTO ICI*-->
						<?php



						?>



					</p>
				</div>
			</section>

			<section id="grid-item2" class="grid-items">
				<div class="description">
					<img src="img/couvertures-livres/livre2.jpg" alt="Boodlad1">
					<livre>
					<p>
						<!--*INSERT INTO ICI*-->
					</p>
				</div>
			</section>

			<section id="grid-item3" class="grid-items">
				<div class="description">
					<img src="img/couvertures-livres/livre3.jpg" alt="Deadman Wonderland 1">
					<p>
						<!--*INSERT INTO ICI*-->
					</p>
				</div>
			</section>

			<section id="grid-item4" class="grid-items">
				<div class="description">
					<img src="img/couvertures-livres/livre4.jpg" alt="Death Note 1">
					<p>
						<!--*INSERT INTO ICI*-->
					</p>
				</div>
			</section>

			<section id="grid-item5" class="grid-items">
				<div class="description">
					<img src="img/couvertures-livres/livre5.jpg" alt="Harry Potter 4">

						<!--*INSERT INTO ICI*-->



				<?php
					$affichage = $bdd->query("SELECT titre, nom, prenom, libelle,
																		FROM livre
																	  JOIN auteur ON livre.isbn = auteur.idLivre
																		JOIN personne ON auteur.idPersonne = personne.id
																		JOIN genre ON livre.genre = genre.id
																		WHERE titre LIKE 'Harry Potter' ");

				 ?>

						<table>

				<?php
					while ($donne = $affichage->fetch()) {
					?>

							<tr>
				        <td><?php echo $donne['titre']?></td>
				        <td><?php echo $donne['nom']?></td>
				        <td><?php echo $donne['prenom']?></td>
				        <td><?php echo $donne['libelle']?></td>
							</tr>

					<?php } ?>

					</table>


				</div>
			</section>

			<section id="grid-item6" class="grid-items">
				<div class="description">
					<img src="img/couvertures-livres/livre6.jpg" alt="Monochrome Animals 1">
					<p>
						<!--*INSERT INTO ICI*-->
					</p>
				</div>
			</section>

			<section id="grid-item7" class="grid-items">
				<div class="description">
					<img src="img/couvertures-livres/livre7.jpg" alt="Prison Lab 1">
					<p>
						<!--*INSERT INTO ICI*-->
					</p>
				</div>
			</section>

			<section id="grid-item8" class="grid-items">
				<div class="description">
					<img src="img/couvertures-livres/livre8.jpg" alt="Switch Girl 1">
					<p>
						<!--*INSERT INTO ICI*-->


					</p>
				</div>
			</section>

			<section id="grid-item9" class="grid-items">
				<div class="description">
					<img src="img/couvertures-livres/livre9.jpg" alt="Hunger Games 1">
					<p>
						<!--*INSERT INTO ICI*-->
					</p>
				</div>
			</section>
		</main>
	</body>
</html>

<!--INSERT INTO *nom_de_la_table*()-->


<!--
	Quand on ouvre une des pages de la bibliothèque, on a un aperçu avec:
		Image
-->
